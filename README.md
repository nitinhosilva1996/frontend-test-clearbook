# Teste Front-End

## :information_source: How To Use

```bash
# Clone this repository
$ git clone https://gitlab.com/nitinhosilva1996/frontend-test-clearbook.git

# Go into the repository
$ cd frontend-test-clearbook

# Install dependencies
$ yarn install

# Run project
$ yarn start
```
