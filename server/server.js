/**
 * Rodar script com node >= 14;
 *
 * Faça uma requisição do tipo POST para /endpoint com um body do tipo JSON no seguinte formato
 *
 * {
 *      "cpf": "123456789",
 *      "birthdate": "2018-01-01",
 *      "email": "conta@dominio.com.br",
 *      "password": "Abc12345"
 *  }
 *
 * Respostas:
 *
 * 200 - { "success": true } para as requisições corretas;
 * 500 - { "error": true } caso o email seja igual a "error@dominio.com.br";
 * 400 - "invalid request" para requisições inválidas;
 */

const http = require("http");
const host = "localhost";
const port = 8080;

const validateRequest = (req) => {
  return req.method === "POST" && req.url === "/endpoint";
};

const validateBody = (data) => {
  try {
    const body = JSON.parse(data);
    return !!body.birthdate && !!body.cpf && !!body.password && !!body.email;
  } catch (err) {
    return false;
  }
};

const requestListener = function (req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Request-Method", "*");
  res.setHeader("Access-Control-Allow-Methods", "OPTIONS, GET");
  res.setHeader("Access-Control-Allow-Headers", "*");
  if (req.method === "OPTIONS") {
    res.writeHead(200);
    res.end();
    return;
  }
  if (!validateRequest(req)) {
    res.writeHead(400);
    return res.end("invalid request");
  }

  let data = "";

  req.on("data", (chunk) => {
    data += chunk;
  });

  req.on("end", () => {
    if (!validateBody(data)) {
      res.writeHead(400);
      return res.end("invalid request");
    }

    const body = JSON.parse(data);
    const success = body.email !== "error@dominio.com.br";
    const error = !success;
    res.setHeader("Content-Type", "application/json");
    res.writeHead(success ? 200 : 500);
    res.end(JSON.stringify(success ? { success } : { error }));
    res.end();
  });
};

const server = http.createServer(requestListener);
server.listen(port, host, () => {
  console.log(`Server is running on http://${host}:${port}`);
});
