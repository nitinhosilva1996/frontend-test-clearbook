import React from 'react';
import ptBr from 'antd/es/locale/pt_BR';
import MaskedInput from 'antd-mask-input';
import { Form, Input, DatePicker, ConfigProvider } from 'antd';

import styles from './CustomInput.module.css';

const CustomInput = props => {
  const { formik, field, label, placeholder, mask, type, info } = props;
  const { errors, values, setFieldValue } = formik;

  const defaultProps = {
    className: styles.input,
    name: field,
    value: values[field],
    placeholder,
    allowClear: true,
    onChange: ({ target }) => setFieldValue(field, target.value),
  };

  function renderInput(type) {
    switch (type) {
      case 'password':
        return <Input.Password {...defaultProps} />;
      case 'mask':
        return <MaskedInput {...defaultProps} mask={mask} />;
      case 'date':
        return (
          <DatePicker
            {...defaultProps}
            format="DD/MM/YYYY"
            onChange={date => setFieldValue(field, date)}
          />
        );
      default:
        return <Input {...defaultProps} />;
    }
  }

  return (
    <Form.Item
      label={label}
      validateStatus={errors[field] ? 'error' : 'validating'}
      help={errors[field]}
    >
      <ConfigProvider locale={ptBr}>
        {renderInput(type)}
        <span className={styles.info}>{info}</span>
      </ConfigProvider>
    </Form.Item>
  );
};

export default CustomInput;
