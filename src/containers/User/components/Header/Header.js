import book from '../../../../assets/icons/signature-book.png';
import styles from './Header.module.css';

const Header = () => (
  <div className={styles.container}>
    <img className={styles.img} src={book} alt="book-icon" />
    <h1 className={styles.h1}>Abra sua conta na Clearbook</h1>
    <h3 className={styles.h3}>
      Em poucos minutos você abre a sua conta e pode investir em projetos
      inovadores
    </h3>
  </div>
);

export default Header;
