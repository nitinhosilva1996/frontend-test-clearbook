import styles from './ScreenInfo.module.css';

const ScreenInfo = props => {
  const { title, description } = props;

  return (
    <div className={styles.container}>
      <h1 className={styles.h1}>{title}</h1>
      <h3 className={styles.h3}>
        {description || 'Todos os campos são obrigatórios'}
      </h3>
    </div>
  );
};

export default ScreenInfo;
