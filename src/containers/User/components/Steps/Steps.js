import React from 'react';
import { Steps } from 'antd';

import useWindowDimensions from '../../../../hooks/useWindowDimensions';

import styles from './Steps.module.css';

const { Step } = Steps;

const StepsScreen = ({ currentStep, steps }) => {
  const { width } = useWindowDimensions();

  return (
    <div className={styles.container}>
      <Steps
        className={styles.steps}
        progressDot
        current={currentStep}
        responsive={width < 400}
      >
        {steps.map((step, i) => (
          <Step key={step} {...(currentStep === i && { title: step })} />
        ))}
      </Steps>
    </div>
  );
};

export default StepsScreen;
