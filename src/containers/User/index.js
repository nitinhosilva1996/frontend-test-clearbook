import { Spin } from 'antd';

import Header from './components/Header/Header';
import Steps from './components/Steps/Steps';
import Pages from './pages';

import useFetch from '../../hooks/useFetch';
import useSetState from '../../hooks/useSetState';

import { STEP } from './util/enums';
import { STEPS } from './util/constants';
import { formatDate, isEmpty } from '../../util/functions';

import { CREATE_USER } from '../../services/Api';

function User() {
  const [state, setState] = useSetState({
    currentStep: STEP.ONE,
    steps: STEPS,
    data: {},
    lastFormikValues: {},
  });
  const { loading, request } = useFetch();

  function formatPayload(values) {
    const { email, password, cpf, birth } = isEmpty(values)
      ? state.lastFormikValues
      : values;

    if (!isEmpty(values)) setState({ lastFormikValues: values });

    return {
      cpf: cpf?.replace(/[^\d]/g, ''),
      birthdate: formatDate(birth?.toISOString(), 'date', '-'),
      email,
      password,
    };
  }

  async function submit(values) {
    const nextStep = {
      [STEP.ONE]: STEP.TWO,
      [STEP.TWO]: STEP.THREE,
    };

    try {
      if ([STEP.TWO, STEP.THREE].includes(state.currentStep)) {
        const { url, options } = CREATE_USER(formatPayload(values));
        const { response } = await request(url, options);

        setState({ data: { success: response.ok } });
      }

      if (state.currentStep !== STEP.THREE) {
        setState({ currentStep: nextStep[state.currentStep] });
      }
    } catch (error) {
      setState({ data: error });
    }
  }

  return (
    <div className="container">
      <Header />
      <Steps {...state} />
      {loading ? (
        <Spin size="large" />
      ) : (
        <Pages {...state} setState={setState} submit={submit} />
      )}
    </div>
  );
}

export default User;
