import React from 'react';

import CustomInput from '../../../components/Form/CustomInput';

const Step1 = ({ formik }) => {
  const info = 'Usaremos este e-mail para falar com você';

  return (
    <>
      <CustomInput field="email" label="E-mail" formik={formik} info={info} />
      <CustomInput
        type="password"
        field="password"
        label="Senha"
        formik={formik}
      />
    </>
  );
};

export default Step1;
