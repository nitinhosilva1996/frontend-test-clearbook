import React from 'react';

import CustomInput from '../../../components/Form/CustomInput';

const Step2 = ({ formik }) => {
  return (
    <>
      <CustomInput
        type="mask"
        field="cpf"
        label="CPF"
        placeholder="000.000.000-00"
        mask="111.111.111-11"
        formik={formik}
      />
      <CustomInput
        type="date"
        field="birth"
        label="Data de nascimento"
        formik={formik}
      />
    </>
  );
};

export default Step2;
