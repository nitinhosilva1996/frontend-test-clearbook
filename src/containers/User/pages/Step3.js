import { Button } from 'antd';
import { STEP } from '../util/enums';

import Success from '../../../assets/images/success.png';
import Error from '../../../assets/images/traffic-cone.png';
import Image from '../../../components/Helper/Image';

import styles from './Pages.module.css';

const Step3 = ({ data, formik, setState }) => {
  function resetSteps() {
    setState({ currentStep: STEP.ONE });

    formik.setValues({});
  }

  return (
    <div className={styles.step3}>
      <Image
        src={data?.success ? Success : Error}
        alt={data?.success ? 'Sucesso no Cadastro' : 'Erro no Cadastro'}
      />

      <Button
        className={styles.step3Btn}
        type="primary"
        onClick={() => (data?.success ? resetSteps() : formik.submitForm())}
      >
        {data?.success ? 'VOLTAR PARA A PÁGINA INICIAL' : 'TENTAR NOVAMENTE'}
      </Button>
    </div>
  );
};

export default Step3;
