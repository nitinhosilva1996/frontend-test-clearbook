import { Form, Button } from 'antd';
import { useFormik } from 'formik';
import * as yup from 'yup';

import ArrowRight from '../../../assets/icons/arrow-right.png';
import ArrowLeft from '../../../assets/icons/arrow-left.png';

import { emailPassword, cpfBirth } from '../util/validations';
import { STEP } from '../util/enums';

import ScreenInfo from '../components/ScreenInfo/ScreenInfo';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';

import styles from './Pages.module.css';

const Pages = ({ currentStep, setState, submit, data }) => {
  const formik = useFormik({
    initialValues: {},
    validateOnBlur: false,
    validateOnChange: false,
    validationSchema: yup.object().shape({
      ...(currentStep === STEP.ONE && emailPassword),
      ...(currentStep === STEP.TWO && cpfBirth),
    }),
    onSubmit: values => {
      submit(values);
    },
  });

  function checkDisabled(currentStep) {
    const { email, password, birth, cpf } = formik.values;
    if (currentStep === STEP.ONE) {
      return !email || !password;
    }

    return !birth || !cpf;
  }

  function getScreenInfoProps(step) {
    if (step === STEP.ONE) return { title: 'Informações de acesso' };
    if (step === STEP.TWO) return { title: 'Dados pessoais' };

    return {
      title: data?.success
        ? 'Cadastro feito com sucesso!'
        : 'Ops, ocorreu um erro!',
      description: data?.success
        ? 'Tudo certo por aqui, você já pode começar a investir.'
        : 'Por favor, tenta novamente.',
    };
  }

  return (
    <div className={styles.container}>
      <ScreenInfo {...getScreenInfoProps(currentStep)} />

      <Form layout="vertical">
        {currentStep === STEP.ONE && <Step1 formik={formik} />}
        {currentStep === STEP.TWO && <Step2 formik={formik} />}
        {currentStep === STEP.THREE && (
          <Step3 formik={formik} setState={setState} data={data} />
        )}
      </Form>

      {currentStep !== STEP.THREE && (
        <div className={styles.buttons}>
          {currentStep === STEP.TWO && (
            <Button
              className={styles.backBtn}
              onClick={() => setState({ currentStep: STEP.ONE })}
            >
              <img src={ArrowLeft} alt="Continuar" />
              VOLTAR
            </Button>
          )}
          <Button
            className={styles.btnContinue}
            disabled={checkDisabled(currentStep)}
            type="primary"
            onClick={() => formik.submitForm()}
          >
            CONTINUAR
            <img src={ArrowRight} alt="Continuar" />
          </Button>
        </div>
      )}
    </div>
  );
};

export default Pages;
