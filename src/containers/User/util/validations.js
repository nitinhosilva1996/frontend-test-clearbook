import * as yup from 'yup';

const required = 'Preenchimento obrigatório.';

const emailPassword = {
  email: yup.string().email('Digite um email válido').required(required),
  password: yup
    .string()
    .matches(/(?=.*[A-Z])(?=.*[0-9]).+/, {
      message: 'A senha deve conter ao menos um número e uma letra maiúscula',
      excludeEmptyString: true,
    })
    .required(required),
};

const cpfBirth = {
  cpf: yup.string().required(required),
  birth: yup.string().required(required),
};

export { emailPassword, cpfBirth };
