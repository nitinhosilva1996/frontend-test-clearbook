import { useState } from 'react';

const useSetState = (initialState = {}) => {
  const [state, set] = useState(initialState);
  const setState = newState =>
    set(prevState => ({ ...prevState, ...newState }));
  return [state, setState];
};

export default useSetState;
