import React from 'react';
import ReactDOM from 'react-dom';

import 'antd/dist/antd.css';
import './global.css';

import App from './routers';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root'),
);
