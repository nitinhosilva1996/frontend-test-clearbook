import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import User from '../containers/User';

export default function App() {
  return (
    <Router>
      <Switch>
        <Route path="/">
          <User />
        </Route>
      </Switch>
    </Router>
  );
}
