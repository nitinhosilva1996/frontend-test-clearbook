export const API_URL = 'http://localhost:8080';

export function CREATE_USER(body) {
  return {
    url: `${API_URL}/endpoint`,
    options: {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    },
  };
}
