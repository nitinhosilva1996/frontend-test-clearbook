// Padrão esperado -> 2021-01-15T21:28
export const formatDate = txt => {
  if (!txt) return undefined;

  return txt.substring(0, 10);
};

export function isEmpty(obj) {
  return Object.keys(obj).length === 0 && obj.constructor === Object;
}
